﻿

#include <stdio.h>

int main()
{
	int a=50;
	//方法一
	int* p = &a;//宣告 指定成a的地址
	//方法二
	int* B ;
	B = &a;
	int* C;
	C = &a;
	

	printf_s("%p\n", &a);//1AF9EC
	printf_s("%d\n", &a);//十進位表示1767916 通常不這樣表示

	printf_s("%d\n",*p);
	printf_s("%d\n", *B);

	printf_s("C=%d\n", C);
	*C = &a;//把C指到的位址的值改成&a
	printf_s("*C = &a  C=%d\n", C);	//讀到C的內容(a的地址
	printf_s("*C = &a *C=%d\n", *C);//讀到a的內容

}
